package com.example.user.test;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageButton;

public class MainActivity extends AppCompatActivity {




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity1);
        ImageButton logo = (ImageButton) findViewById(R.id.logo);
        Animation animation1= AnimationUtils.loadAnimation(this, R.anim.translate);
        logo.startAnimation(animation1);
        logo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImageButton logo = (ImageButton) findViewById(R.id.logo);
                Animation animation2= AnimationUtils.loadAnimation(getApplicationContext(), R.anim.ib_click);
                logo.startAnimation(animation2);
            }
        });
    }
}
